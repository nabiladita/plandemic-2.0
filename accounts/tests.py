from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import *
from .forms import *
from .views import loginPage, logoutUser, registerPage
from django.contrib.auth.models import User
from django.apps import apps
from accounts.apps import AccountsConfig


# Create your tests here.

class UnitTestRegisterLogin(TestCase):

    # Url testcases
    def test_url_is_exist_register(self):
        response = Client().get('/accounts/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_is_exist_login(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    # View testcases
    def test_view_plandemic_is_using_function_registerPage(self):
        found = resolve('/accounts/register/')
        self.assertEqual(found.func, registerPage)

    def test_view_plandemic_is_using_function_loginPage(self):
        found = resolve('/accounts/login/')
        self.assertEqual(found.func, loginPage)

    # Template testcases
    def test_template_register_using_register_template(self):
        response = Client().get('/accounts/register/')
        self.assertTemplateUsed(response, 'register.html') 

    def test_template_login_using_login_template(self):
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, 'login.html')

class SetUp_Class(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="username", email="user@mp.com", password1="halouser", password2="halouser")


class User_Form_Test(TestCase):
       
    # Valid Form Data
    def test_UserForm_valid(self):
        form = CreateUserForm(data={'username':"username", 'email': "user@mp.com", 'password1': "halouser", 'password2': "halouser"})
        self.assertTrue(form.is_valid())

    # Invalid Form Data
    def test_UserForm_invalid(self):
        form = CreateUserForm(data={'username':"username", 'email': "", 'password1': "halo", 'password2': "halouser"})
        self.assertFalse(form.is_valid())

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(AccountsConfig.name, 'accounts')
		self.assertEqual(apps.get_app_config('accounts').name, 'accounts')