from django.test import TestCase, Client
from .views import hello

# Create your tests here.
class TestInformasiWFH(TestCase):
	def test_url_exist(self):
		response = Client().get('/Hello')
		self.assertEqual(response.status_code, 200)

	def test_view(self):
		response = self.client.get('/Hello')
		self.assertTemplateUsed(response, 'index.html')

	def test_content(self):
		response = self.client.get('/Hello')
		self.assertContains(response, 'Hello World')