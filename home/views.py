from django.shortcuts import render,redirect
from .forms import FeedbackForms, FeedbackFormBaru
from .models import FeedbackBaru
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse
from django.template.loader import render_to_string



# Create your views here.
def Home(request):
    return render(request,'Home/Home.html')

def About(request):
    if request.method ==  "POST":
        form1 = FeedbackForms(request.POST)
        if form1.is_valid():
            form1.save()
            return redirect('Home:About')
    else :      
        form = FeedbackForms()
        return render(request, 'Home/about.html',{"form_html" : form})

def Feedback(request):
    feedbacks = FeedbackBaru.objects.all()

    if request.method == "POST":
        form_feedback = FeedbackFormBaru(request.POST or None)
        if form_feedback.is_valid():
            name = request.POST.get('name')
            content = request.POST.get('content')
            feedback = FeedbackBaru.objects.create(name=name, content=content)
            feedback.save()
            # return HttpResponseRedirect('/about')
    else:
        form_feedback = FeedbackFormBaru()

    context = {
        'feedbacks':feedbacks,
        'form_feedback':form_feedback,
    }

    if request.is_ajax():
        html = render_to_string('blog/comments.html', context, request=request)
        return JsonResponse({'form':html})

    return render(request, 'Home/about.html', context)

