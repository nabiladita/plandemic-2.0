from django.test import TestCase,Client
from home.models import FeedbackBaru
from home.forms import FeedbackFormBaru
from home.apps import HomeConfig
from django.apps import apps

# Create your tests here.
class Test(TestCase):
    def test_url_untuk_home(self):
        response = Client().get('')
        self.assertEquals(response.status_code,200)

    def test_url_untuk_about(self):
        response = Client().get('/about')
        self.assertEquals(response.status_code,200)
    
    def test_apakah_ada_plandemic(self):
        response = Client().get('')
        template_name = response.content.decode('utf8')
        self.assertIn("PLANDEMIC",template_name)
    
    def test_apakah_ada_about_us(self):
        response = Client().get('/about')
        template_name = response.content.decode('utf8')
        self.assertIn("About Us",template_name)
        self.assertIn("Hello! We are from Faculty of Computer Science, University of Indonesia.",template_name)
    
    def test_correct_template_home(self): 
        response = Client().get('')
        self.assertTemplateUsed(response,'Home/Home.html')
    
    def test_correct_template_about(self): 
        response = Client().get('/about')
        self.assertTemplateUsed(response,'Home/about.html')

    def test_object_can_be_created(self):
        feedback = FeedbackBaru.objects.create(name='User', content='This web is amazing!')

    def test_object_content_in_database(self):
        feedback = FeedbackBaru.objects.create(name='User', content='This web is amazing!')
        self.assertEqual(feedback.name, 'User')
        self.assertEqual(feedback.content, 'This web is amazing!')

    # def test_object_content_in_web(self):
    #     feedback = FeedbackBaru.objects.create(name='User', content='This web is amazing!')
    #     response = Client().get('/about/')
    #     self.assertContains(response, 'This web is amazing!')

    def test_class_css_from_form(self):
        form_feedback = FeedbackFormBaru()
        self.assertIn('class="form-control"', form_feedback.as_p())


class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(HomeConfig.name, 'home')
		self.assertEqual(apps.get_app_config('informasi_wfh').name, 'informasi_wfh')
