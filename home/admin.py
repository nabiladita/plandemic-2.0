from django.contrib import admin
from .models import Feedback, FeedbackBaru

# Register your models here.
admin.site.register(Feedback)
admin.site.register(FeedbackBaru)