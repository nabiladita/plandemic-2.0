# from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


# Create your models here.
# Create by Zaidan

class Feedback(models.Model):
    id = models.AutoField(primary_key=True,)
    isi_feedback = models.TextField()
    nama_pengisi_feedback = models.CharField(max_length = 50)

    
class FeedbackBaru(models.Model):
    # post = models.ForeignKey(Post)
    name = models.CharField(max_length=100, null=True)
    content = models.CharField(max_length=160)
    time_stamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return 'Feedback-{}'.format(name)
    