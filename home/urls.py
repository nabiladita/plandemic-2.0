from django.urls import path
from home.views import Home, About, Feedback

app_name = 'Home'

urlpatterns=[
    path('',Home,name='Home'),
    path('about',Feedback,name='About'),
]