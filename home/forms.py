from django import forms
from .models import Feedback, FeedbackBaru

class FeedbackForms(forms.ModelForm):
    class Meta : 
        model = Feedback
        fields = ["isi_feedback","nama_pengisi_feedback"]
        widgets = {
            'isi_feedback':forms.Textarea(attrs={'class':'form-control'}),
            'nama_pengisi_feedback':forms.TextInput(attrs={'class':'form-control'})}


class FeedbackFormBaru(forms.ModelForm):
    class Meta:
        model = FeedbackBaru
        fields = '__all__'
        widgets = {
            'name':forms.TextInput(
                attrs={
                    'class':'form-control',
                }
            ),
            'content':forms.Textarea(
                attrs={
                    'class':'form-control',
                },
            )
        }
        labels = {
            "content": "Feedback"
        }


            