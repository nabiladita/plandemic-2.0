from django import forms
from .models import InformasiWFH

class Input_News(forms.ModelForm):
    class Meta:
        model = InformasiWFH
        fields = [
            'title',
            'link',
            'short_Description',
        ]
        widgets = {
            'title' : forms.TextInput(
                attrs={
                'class':'form-style',
                }),
            'link': forms.TextInput(attrs={
                'class':'form-style',
                'placeholder': 'Please start with http://'
                }),
            'short_Description': forms.Textarea(
                attrs={
                    'rows': 7,
                    'class':'form-style',
                    }),
        }


