from django.db import models
from django.forms.widgets import TextInput

# Create your models here.
class InformasiWFH(models.Model):
    title = models.CharField(max_length=100, null=True)
    link = models.URLField(max_length=500, null=True)
    short_Description = models.CharField(max_length=1000, null=True)

