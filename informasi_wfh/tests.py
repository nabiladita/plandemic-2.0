from http import HTTPStatus
from django.test import TestCase, Client
from django.contrib.auth import get_user_model

from informasi_wfh.views import add_informasi_wfh
from informasi_wfh.models import InformasiWFH
from informasi_wfh.forms import Input_News

from django.http import request, HttpResponse
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth import authenticate, login, logout
from accounts.forms import CreateUserForm
from django.apps import apps
from informasi_wfh.apps import InformasiWfhConfig

from selenium import webdriver
import os

# Create your tests here.
class TestInformasiWFH(TestCase):
	def test_url_list_informasi_wfh_exist(self):
		response = Client().get('/wfh/')
		self.assertEqual(response.status_code, 200)

	def test_url_add_news_wfh_exist(self):
		response = Client().get('/wfh/add')
		self.assertEqual(response.status_code, 302)

	def test_correct_template_list_informasi_wfh(self):
		response = self.client.get('/wfh/')
		self.assertTemplateUsed(response, 'wfh/list.html')

	def test_object_can_be_created(self):
		berita1 = InformasiWFH.objects.create(title = 'Berita 1', link='https://www.antaranews.com/berita/1803229/tips-hemat-kuota-internet-selama-wfh', short_Description='Jakarta (ANTARA) - Pemakaian internet memang naik tajam sejak pandemi virus corona di Indonesia, banyak orang yang harus tetap berada di dalam rumah untuk bekerja dan belajar. Sebanyak 55,6 responden mengaku kebutuhan internet mereka naik...')

	def test_object_content_in_database(self):
		berita1 = InformasiWFH.objects.create(title = 'Berita 1', link='https://www.antaranews.com/berita/1803229/tips-hemat-kuota-internet-selama-wfh', short_Description='Jakarta (ANTARA) - Pemakaian internet memang naik tajam sejak pandemi virus corona di Indonesia, banyak orang yang harus tetap berada di dalam rumah untuk bekerja dan belajar. Sebanyak 55,6 responden mengaku kebutuhan internet mereka naik...')
		self.assertEqual(berita1.title, 'Berita 1')
		self.assertEqual(berita1.link, 'https://www.antaranews.com/berita/1803229/tips-hemat-kuota-internet-selama-wfh')
		self.assertIn('Jakarta (ANTARA) - Pemakaian internet', berita1.short_Description)

	def test_object_content_in_web(self):
		berita1 = InformasiWFH.objects.create(title = 'Berita 1', link='https://www.antaranews.com/berita/1803229/tips-hemat-kuota-internet-selama-wfh', short_Description='Jakarta (ANTARA) - Pemakaian internet memang naik tajam sejak pandemi virus corona di Indonesia, banyak orang yang harus tetap berada di dalam rumah untuk bekerja dan belajar. Sebanyak 55,6 responden mengaku kebutuhan internet mereka naik...')
		response = Client().get('/wfh/')
		self.assertContains(response, 'Jakarta (ANTARA) - Pemakaian internet')

	def test_class_css_from_form(self):
		form = Input_News()
		self.assertIn('class="form-style"', form.as_p())

class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(InformasiWfhConfig.name, 'informasi_wfh')
		self.assertEqual(apps.get_app_config('informasi_wfh').name, 'informasi_wfh')





		




