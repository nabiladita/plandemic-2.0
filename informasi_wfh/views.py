from django.shortcuts import render, redirect
from django.urls import reverse

from .models import InformasiWFH
from informasi_wfh.forms import Input_News

from django.contrib.auth.decorators import login_required

from django.http import JsonResponse
from django.forms.models import model_to_dict

# Create your views here.
def list_informasi_wfh(request):
	form = Input_News(request.POST or None)
	list_informasi = InformasiWFH.objects.all()

	if form.is_valid():
		informasi = form.save()
		return JsonResponse({'informasi':model_to_dict(informasi)}, status=200)
	
	response = {'form' : form ,
				'list_informasi': list_informasi}

	return render(request, 'wfh/list.html', response)

@login_required(login_url='/accounts/login')
def add_informasi_wfh(request):
	form = Input_News(request.POST or None)
	if form.is_valid():
			form.save()
			return redirect("/wfh")
	else:
		response = { 'form' : form }
		return render(request, 'wfh/add-wfh.html', response)