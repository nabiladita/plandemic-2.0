from django.urls import path	

from . import views

app_name = 'informasi_wfh'

urlpatterns = [
    path('', views.list_informasi_wfh, name='list_informasi_wfh'),
    path('add', views.add_informasi_wfh, name='add_informasi_wfh'),
]