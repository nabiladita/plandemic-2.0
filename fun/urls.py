from django.urls import path	
from . import views


app_name = 'fun'

urlpatterns = [
    path('', views.fun_home, name='fun_home'),
] 