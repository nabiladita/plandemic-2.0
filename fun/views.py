from django.shortcuts import render,redirect
from .forms import fun_form
from .models import fun_model
from django.contrib.auth.decorators import login_required  
from django.http import JsonResponse
from django.forms.models import model_to_dict

# Create your views here.

def get_fun(request):
    form = fun_form()
    fun_object = fun_model.objects.all()

    response={
        'form' :form,
        'fun_object' : fun_object
    }

    return render(request,'fun_front/fun_home.html',response)

def fun_home(request):
    form = fun_form(request.POST or None)
    fun_object = fun_model.objects.all()

    if form.is_valid():
        new_form=form.save()
        return JsonResponse({'fun':model_to_dict(new_form)}, status=200)

    else:
        context = {
            'form' : form,
            'fun_object' : fun_object
        }

    return render(request,'fun_front/fun_home.html',context)

@login_required(login_url='/accounts/login')
def add_fun(request):
    form = fun_form(request.POST, request.FILES)
    model = fun_model

    if form.is_valid():
        form.save()

        context={
            'form':form
        }

        return redirect('fun:fun_home')

    else:
        context = {
            'form' : form
        }

    return render(request,'fun_front/fun_home.html',context)

