from django.test import TestCase,Client
from .forms import fun_form
from .models import fun_model
# Create your tests here.

class Test(TestCase):
    def test_url_untuk_url_fun(self):
        response = Client().get('/fun/')
        self.assertEquals(response.status_code,200)

    def test_template_fun(self): 
        response = Client().get('/fun/')
        self.assertTemplateUsed(response,'fun_front/fun_home.html')
    
    def test_html_fun(self):
        response = Client().get('/fun/')
        template_name = response.content.decode('utf8')
        self.assertIn("To Do To Have Fun",template_name)
        self.assertIn("Let's Recommend Something",template_name)
    
    def test_model_can_create_new_recommendation(self):
        new_activity = fun_model.objects.create(Title='test',Link='test')
        counting_all_available_news = fun_model.objects.all().count()
        self.assertEqual(counting_all_available_news, 1)