from django import forms
from .models import fun_model

class fun_form(forms.ModelForm):
    class Meta:
        model = fun_model
        fields = [
            'Title',
            'Link',
            # 'Image',
        ]
