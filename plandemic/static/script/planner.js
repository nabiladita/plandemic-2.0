$( document ).ready(function() {
  $("#addplan-form" ).submit(function( event ) {
    event.preventDefault();
    $.ajax({
        type        : 'POST',
        url         : '/planner/', 
        data        : $('#addplan-form').serialize(), 
        dataType    : 'json', 
        success: function(response) {
          console.log(response)
          $("#planlist").empty();
          for(i=0; i< response.length; i++){
            var title = response[i].fields.Title;
            var time = response[i].fields.Time;
            var description = response[i].fields.Description;
            $("#planlist").append(
              '<div class="card" id=card-planner>'+
              '<h6 class="card-header">'+ title + '</h6>'+
              '<div class="card-body">'+
                '<h5 class="card-title">' + time +'</h5>'+
                '<p class="card-text">'+ description + '</p>'+
              '</div>'+
            '</div>'
            )
          }
        }
      })
      $("#addplan-form")[0].reset();
  });
$('#addplan-toggle').on('click', function(event){
    event.preventDefault();
    var accordion = $(this);
    var content = accordion.next('.accordion-content');
    
    accordion.toggleClass("open");
    content.slideToggle(100);
});
$("#addplan-toggle").hover(function(){
    $(this).css("background-color", "#7357b4");
    }, function(){
    $(this).css("background-color", "#2D91DE");
  });

$(".card-header").hover(function(){
    $(this).css("background-color", "#2D91DE");
    }, function(){
    $(this).css("background-color", "#754798");
  });

  $("#keyword").keyup(function(){
    var ketikan = $("#keyword").val();
    $.ajax({
        url : "/planner/data?q=" + ketikan,
        success: function(data) {
            console.log(data);
            $("#planlist").empty();
            for(i=0; i< data.length; i++){
              var title = data[i].fields.Title;
              var time = data[i].fields.Time;
              var description = data[i].fields.Description;
              $("#planlist").append(
                '<div class="card" id=card-planner>'+
                '<h6 class="card-header">'+ title + '</h6>'+
                '<div class="card-body">'+
                  '<h5 class="card-title">' + time +'</h5>'+
                  '<p class="card-text">'+ description + '</p>'+
                '</div>'+
              '</div>'
            )
            }
        }
    });
});
});
