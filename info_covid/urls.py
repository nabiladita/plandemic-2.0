from django.urls import path	

from . import views

app_name = 'info_covid'

urlpatterns = [
    path('', views.covid_per_country, name='covid_case_per_country'),
    path('country/', views.country_detail, name='country_detail'),
]