from django.test import TestCase, Client
from django.urls import reverse
from info_covid.models import DataPerCountry
from django.contrib.auth.models import User

# Create your tests here.
class TestInformasiWFH(TestCase):
	def test_url_covid(self):
		response = Client().get('/covid/')
		self.assertEqual(response.status_code, 200)

	def test_correct_template_info_covid(self):
		response = self.client.get('/covid/')
		self.assertTemplateUsed(response, 'covid/info_covid_per_country.html')

	def test_10_country_without_login(self):
		response = self.client.get('/covid/')
		self.assertEqual(len(response.context['database_covid']), 10)

	def test_234_country_with_login(self):
		c = Client()
		self.user = User.objects.create_user(username = 'tsukki', password="moon")
		self.client.login(username = 'tsukki', password="moon")

		response = self.client.get('/covid/')
		self.assertEqual(len(response.context['database_covid']), 234)

	def test_more(self):
		response = self.client.get('/covid/')
		self.assertContains(response, 'Want to see more country?')
		self.assertContains(response, 'Login Now!')