from django import forms
from .models import DataPerCountry

class SearchBar(forms.ModelForm):
    class Meta:
        model = DataPerCountry
        fields = [
            'name',
        ]
        widgets = {
            'name' : forms.TextInput(
                attrs={
                'class':'form-style',
                'placeholder':'Search by country name',
                }
            )
        }

