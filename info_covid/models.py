from django.db import models

# Create your models here.
class DataPerCountry(models.Model) :
    name = models.CharField(max_length=100)
    region = models.CharField(max_length=100)
    total_cases = models.IntegerField()
    total_cases_per_million = models.FloatField(default=None)
    total_deaths = models.IntegerField()
    total_deaths_per_million = models.FloatField(default=None)
    transmission = models.CharField(max_length=100)

    def __str__(self):
        return self.name



    

   

