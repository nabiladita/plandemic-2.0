# Generated by Django 3.1.2 on 2020-11-20 04:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('info_covid', '0004_auto_20201120_1110'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='datapercountry',
            name='daily_death_cases',
        ),
        migrations.RemoveField(
            model_name='datapercountry',
            name='daily_new_cases',
        ),
        migrations.RemoveField(
            model_name='datapercountry',
            name='weekly_death_cases',
        ),
        migrations.RemoveField(
            model_name='datapercountry',
            name='weekly_new_cases',
        ),
    ]
