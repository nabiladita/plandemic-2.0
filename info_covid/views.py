from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages

from .models import DataPerCountry
from info_covid.forms import SearchBar


import csv
import csv23

import os

def covid_per_country(request):
	#import data from csv file to database once
	get_data = DataPerCountry.objects.all().first()
	data_was_created = True if (get_data != None) else False

	if data_was_created==False :
		with csv23.open_reader(os.path.join(os.path.dirname(__file__), 'covid19-per-country-20-11-2020.csv')) as reader:
			for row in reader:
				created = DataPerCountry(
					name = row[0],
					region = row[1],
					total_cases = row[2],
					total_cases_per_million = row[3],
					total_deaths = row[4],
					total_deaths_per_million = row[5],
					transmission = row[6]
					)
				created.save()

	#form
	form = SearchBar(request.GET or None)

	if request.user.is_authenticated:
		database = DataPerCountry.objects.all().reverse()
	else:
		database = DataPerCountry.objects.all().reverse()[:10]

	response = {
		'database_covid': database,
		'form': form
	}
	return render(request, 'covid/info_covid_per_country.html', response)

def country_detail(request):
	query = request.GET.get('name')
	country = DataPerCountry.objects.filter(name=query.title())
	try:
		check = country[0]
		response={
			'country':country
		}
		return render(request, 'covid/country_details.html', response)
	except(IndexError):
		messages.error(request, "Country not found!")
		return redirect('/covid/')
	
