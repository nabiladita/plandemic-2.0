# Tugas Kelompok 2 Kelompok A16 PPW

<!-- ![Test and Deploy][actions-badge] -->
![pipeline status][pipeline-badge]
![coverage report][coverage-badge]

Anggota Kelompok : <br />
● Aisha Salsabila - 1906399902 <br />
● Alisha Yumna Bakri - 1906400173 <br />
● Kaysa Syifa Wijdan Amin - 1906400362 <br />
● M. Zaidan Ariiq - 1906400394 <br />
● Nabila Dita Putri - 1906400085 <br />

Link HerokuApp : https://plandemic-2.herokuapp.com/
<br />
Deskripsi Web Aplikasi :
Project web aplikasi yang kami buat merupakan aplikasi yang memiliki fitur untuk menampilkan informasi mengenai Learn From Home, Work From Home, dan menampilkan informasi mengenai COVID-19, serta planner yang dapat digunakan untuk menuliskan rencana kegiatan maupun deadline. 
<br />
<br />
Fitur Web Aplikasi : <br />
Planner :
- login/signup/logout <br />
- new Plan <br />
- Plan description
<br />
Informasi Learn From Home : <br />
- add informasi <br />
- menampilkan informasi terkait Learn From Home
<br />
Informasi Work From Home : <br /> 
- add informasi <br />
- menampilkan informasi terkait Work From Home
<br />
Informasi Pandemi : <br />
- add informasi <br />
- menampilkan informasi terkait Pandemi Covid-19


<!-- [actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg -->
[pipeline-badge]: https://gitlab.com/nabiladita/plandemic-2.0/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/nabiladita/plandemic-2.0/badges/master/coverage.svg 
