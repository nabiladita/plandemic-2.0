from django.test import TestCase, Client
from.models import plan
from .views import fungsi_data
from django.urls import resolve
from django.contrib.auth.models import User
from http import HTTPStatus
from planner.apps import PlannerConfig
class TestPlanner(TestCase):
    # Url testcases
    def test_func(self):
        found = resolve('/planner/data/')
        self.assertEqual(found.func, fungsi_data)

    def test_objek_dibuat(self):
        my_user = User.objects.create(username='Testuser')
        plan.objects.create(Title='Belajar',Time='Januari 12 2021',
        Description='Di Perpusat',user=my_user)
        counter = plan.objects.all().count()
        self.assertEqual(counter,1)
    
    def planner_after_login(self):
        user = Client().post('accounts/register/', {
            'username': 'userbaru',
            'email': 'satepadang',
            'password1': 'satepadang',
            'password2': 'satepadang'
            })
        user.client.login(username='userbaru', password='satepadang')
        response = user.client.get('/planner/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)

    def test_get_returns_json(self):
        my_user = User.objects.create(username='Testuser')
        plan.objects.create(Title='Belajar',Time='Januari 12 2021',
        Description='Di Perpusat',user=my_user)
        response = self.client.get('/informative-page/data?q=Belajar')
        self.assertEqual(response['content-type'], "text/html")
        
    def url_planner(self):
        response = Client().get('/planner/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        
    def test_apps(self):
        self.assertEqual(PlannerConfig.name, 'planner')

    def test_Title(self):
        my_user = User.objects.create(username='Testuser')
        plan.objects.create(Title='Belajar',Time='Januari 12 2021',
        Description='Di Perpusat',user=my_user)
        objek = plan.objects.get(id = 1)
        max_length = objek._meta.get_field('Title').max_length
        self.assertEqual(max_length, 30)

    def test_Time(self):
        my_user = User.objects.create(username='Testuser')
        plan.objects.create(Title='Belajar',Time='Januari 12 2021',
        Description='Di Perpusat',user=my_user)
        objek = plan.objects.get(id = 1)
        max_length = objek._meta.get_field('Time').max_length
        self.assertEqual(max_length, 30)

    def test_Description(self):
        my_user = User.objects.create(username='Testuser')
        plan.objects.create(Title='Belajar',Time='Januari 12 2021',
        Description='Di Perpusat',user=my_user)
        objek = plan.objects.get(id = 1)
        max_length = objek._meta.get_field('Description').max_length
        self.assertEqual(max_length, 100)