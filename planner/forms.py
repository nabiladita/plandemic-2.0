from django import forms
from django.forms import ModelForm, DateInput

from .models import plan

class PostForm(forms.ModelForm):
    class Meta:
        model = plan
        fields = [
            'Title',
            'Time',
            'Description'
        ]

        widgets = {
            'Title': forms.TextInput(attrs={'class': 'form-control',}),
            'Time' : forms.TextInput(attrs={'class': 'form-control',}),
            'Description': forms.Textarea(attrs={'class': 'form-control',}),
        }
        exclude = ['user']

