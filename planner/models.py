from django.db import models
import datetime
from django.contrib.auth.models import User

# Create your models here.
class plan(models.Model):
    Title = models.CharField(max_length=30)
    Time = models.CharField(max_length=30)
    Description = models.TextField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.Title