from django.shortcuts import render, redirect
from .models import plan
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .forms import PostForm
from django.http import HttpResponseRedirect
from django.views.generic import View

from django.http import JsonResponse
from django.forms.models import model_to_dict
from django.core import serializers

from django.http import HttpResponse
from django.http import JsonResponse


class Listplan(View):
    def get(self, request):
        if request.user.is_authenticated:
            log_user = request.user
            form = PostForm()
            plans = plan.objects.filter(user=log_user)
            return render(request, 'planner.html', {'form': form, 'plans':plans})
        else:
            return redirect('/accounts/login')

    def post(self,request):
        if request.user.is_authenticated:
            log_user = request.user
            plans = plan.objects.all()
            form = PostForm(request.POST)
            if request.is_ajax():
                title = form.data['Title']
                time = form.data['Time']
                description = form.data['Description']
                plans.create(
                    Title=title,
                    Time=time,
                    Description=description,
                    user=request.user,
                )
                planuser = plan.objects.filter(user=log_user)
                planslist = serializers.serialize('json', planuser)
                return HttpResponse(planslist, content_type="application/json")
            else:
                return HttpResponseRedirect('/planner/')

        else:
            return redirect('/accounts/login')
        
def fungsi_data(request):
    ketikan = request.GET['q']
    if ketikan!="":
        log_user = request.user
        plans = plan.objects.filter(user=log_user, Title = ketikan)
        planslist = serializers.serialize('json', plans)
        return HttpResponse(planslist, content_type="application/json")
    else:
        log_user = request.user
        plans = plan.objects.filter(user=log_user)
        planslist = serializers.serialize('json', plans)
        return HttpResponse(planslist, content_type="application/json")


