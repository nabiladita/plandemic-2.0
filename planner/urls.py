from django.urls import path
from .views import Listplan, fungsi_data
from . import views

app_name = 'planner'

urlpatterns = [
    path('', Listplan.as_view(),name="planner"),
    path('data/',views.fungsi_data,name='data'),
]