
from django import forms
from .models import Form_Add_News_PJJ

class Add_News_PJJ_Form(forms.ModelForm):
    class Meta:
        model = Form_Add_News_PJJ
        fields = [
            'Title',
            'Link',
            'Short_Description'
        ]

       