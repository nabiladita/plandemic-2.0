from django.db import models

# Create your models here.
class Form_Add_News_PJJ(models.Model):
    Title = models.CharField(max_length=200)
    Link = models.URLField(max_length = 200)
    Short_Description = models.TextField()