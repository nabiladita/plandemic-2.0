from django.test import TestCase,Client
from .forms import Add_News_PJJ_Form
from .models import Form_Add_News_PJJ
# Create your tests here.
class Test(TestCase):
    def test_url_untuk_add_news_info_pjj(self):
        response = Client().get('/Information/Add_Information_Learn_From_Home')
        self.assertEquals(response.status_code,302)
    
    #def test_add_news_pjj_ada_html(self): 
        #response = Client().get('/Information/Add_Information_Learn_From_Home')
        #self.assertTemplateUsed(response,'info_PJJ/Add_News_PJJ.html')
    
    def test_apakah_ada_add_news_dan_tombol_add(self):
        response = Client().get('/Information/Add_Information_Learn_From_Home')
        template_name = response.content.decode('utf8')
        self.assertIn("Add News",template_name)
        self.assertIn("Title",template_name)
        self.assertIn("Link",template_name)
        self.assertIn("Description",template_name)
        self.assertIn("ADD",template_name)
    
    #def test_apakah_html_add_news_implement_css(self):
    
    def test_model_can_create_new_news_learn_from_home(self):
        new_activity = Form_Add_News_PJJ.objects.create(Title='test',Link='test',Short_Description='test')
        counting_all_available_news = Form_Add_News_PJJ.objects.all().count()
        self.assertEqual(counting_all_available_news, 1)

    def test_url_untuk_information_learn_from_home(self):
        response = Client().get('/Information/Learn_From_Home')
        self.assertEquals(response.status_code,200)
    
    def test_learn_from_home_ada_html(self): 
        response = Client().get('/Information/Learn_From_Home')
        self.assertTemplateUsed(response,'info_PJJ/Learn_From_Home.html')
    
    def test_apakah_ada_add_news_dan_tombol_add(self):
        response = Client().get('/Information/Learn_From_Home')
        template_name = response.content.decode('utf8')
        self.assertIn("Information About Learn From Home",template_name)
        self.assertIn("Add More Information",template_name)
        # self.assertIn("Read More",template_name)

    #def test_apakah_html_punya_bootstrap(self):

    #def test_apakah_html_implement_css(self):