from django.urls import path
from info_PJJ.views import Add_News_PJJ,Learn_From_Home

app_name = 'info_PJJ'

urlpatterns=[
    path('Add_Information_Learn_From_Home',Add_News_PJJ,name='Add_News_PJJ'),
    path('Learn_From_Home',Learn_From_Home,name='Learn_From_Home'),
]