from django.shortcuts import render,redirect
from .forms import Add_News_PJJ_Form
from .models import Form_Add_News_PJJ
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/accounts/login')
def Add_News_PJJ(request):
    form = Add_News_PJJ_Form()
    model = Form_Add_News_PJJ

    if request.method=="POST":
        form=Add_News_PJJ_Form(request.POST)
        if form.is_valid():
            Title=request.POST.get("Title")
            Link=request.POST.get("Link")
            Short_Description=request.POST.get("Short_Description")
            form.save()

        context={
            'form':form
        }

        return redirect('info_PJJ:Learn_From_Home')

    else:
        context = {
            'form' : form
        }

    return render(request,'info_PJJ/Add_News_PJJ.html',context)

def Learn_From_Home(request):
    model = Form_Add_News_PJJ

    News = Form_Add_News_PJJ.objects.all()
 
    response={
        'News' : News
    }

    return render(request,'info_PJJ/Learn_From_Home.html',response)