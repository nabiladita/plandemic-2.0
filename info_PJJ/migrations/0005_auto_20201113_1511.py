# Generated by Django 3.1.2 on 2020-11-13 08:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('info_PJJ', '0004_auto_20201112_1317'),
    ]

    operations = [
        migrations.RenameField(
            model_name='form_add_news_pjj',
            old_name='Name',
            new_name='Title',
        ),
    ]
